import ezdxf
import numpy as _np
import re


doc = ezdxf.new('R2000')  # hatch requires the DXF R2000 (AC1015) format or later
msp = doc.modelspace()  # adding entities to the model space

hatch = msp.add_hatch(color=255)  # by default a solid fill hatch with fill color=7 (white/black)
hatch = msp.add_hatch(color=5, dxfattribs={
    'hatch_style': ezdxf.const.HATCH_STYLE_NESTED,
    # 0 = nested: ezdxf.const.HATCH_STYLE_NESTED
    # 1 = outer: ezdxf.const.HATCH_STYLE_OUTERMOST
    # 2 = ignore: ezdxf.const.HATCH_STYLE_IGNORE
})
# every boundary path is always a 2D element
# vertex format for the polyline path is: (x, y[, bulge])
# there are no bulge values in this example
# hatch.paths.add_polyline_path([(0, 0), (10, 0), (10, 10), (0, 10)])
# hatch.paths.add_polyline_path(
#     [(0, 0), (3, 0), (3, 3), (0, 3)], is_closed=True,
#     flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL)
_rh = 0.25
_gap = [_np.nan, _np.nan]
PV = {
    'name': 'PV',
    'paths': [(0, 0), (0, _rh), (_rh * 6, _rh), (_rh * 6, -_rh),
              (0, -_rh), _gap, (_rh * 6, 0)],
    'anchors': {'center': [_rh, 0]},
    'shapes': [{'shape': 'poly',
                'xy': _np.array([[0, _rh], [_rh * 1.4, 0], [0, -_rh]]),
                'fill': False}]
}
hatch.paths.add_polyline_path(
    PV, is_closed=True,
    flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST)
# hatch.paths.add_polyline_path(
#     [(2, 2), (8, 2), (8, 8), (2, 8)], is_closed=True,
#     flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL)
lwpolyline = msp.add_lwpolyline(
    [(0, 0, 0), (10, 0, .5), (10, 10, 0), (0, 10, 0)],
    format='xyb',
    close=True,
)

hatch = msp.add_hatch(color=1)
path = hatch.paths.add_polyline_path(
    # get path vertices from associated LWPOLYLINE entity
    lwpolyline.get_points(format='xyb'),
    # get closed state also from associated LWPOLYLINE entity
    is_closed=lwpolyline.closed,
)

# Set association between boundary path and LWPOLYLINE
hatch.associate(path, [lwpolyline])
hatch.paths.add_polyline_path([
    (240, 210, 0),
    (0, 210, 0),
    (0, 0, 0.),
    (240, 0, 0),
],
    is_closed=1,
    flags=ezdxf.const.BOUNDARY_PATH_EXTERNAL,
)
# 2. edge path
edge_path = hatch.paths.add_edge_path(flags=ezdxf.const.BOUNDARY_PATH_OUTERMOST)
edge_path.add_spline(
    control_points=[
        (126.658105895725, 177.0823706957212),
        (141.5497003747484, 187.8907860433995),
        (205.8997365206943, 154.7946313459515),
        (113.0168862297068, 117.8189380884978),
        (202.9816918983783, 63.17222935389572),
        (157.363511042264, 26.4621294342132),
        (144.8204003260554, 28.4383294369643)
    ],
    knot_values=[
        0.0, 0.0, 0.0, 0.0, 55.20174685732758, 98.33239645153571,
        175.1126541251052, 213.2061566683142, 213.2061566683142,
        213.2061566683142, 213.2061566683142
    ],
)
edge_path.add_arc(
    center=(152.6378550678883, 128.3209356351659),
    radius=100.1880612627354,
    start_angle=94.4752130054052,
    end_angle=177.1345242028005,
)
edge_path.add_line(
    (52.57506282464041, 123.3124200796114),
    (126.658105895725, 177.0823706957212)
)
hatch.paths.add_polyline_path(
    [(3, 3), (7, 3), (7, 7), (3, 7)], is_closed=True,
    flags=ezdxf.const.BOUNDARY_PATH_DEFAULT)
edge_path = hatch.paths.add_edge_path()
# each edge path can contain line arc, ellipse and spline elements
# important: major axis >= minor axis (ratio <= 1.)
edge_path.add_ellipse((0, 0), major_axis=(0, 10), ratio=0.5)
# helper function
def print_entity(e):
    print("LINE on layer: %s\n" % e.dxf.layer)
    print("start point: %s\n" % e.dxf.start)
    print("end point: %s\n" % e.dxf.end)

# iterate over all entities in modelspace
msp = doc.modelspace()
for e in msp:
    if e.dxftype() == 'LINE':
        print_entity(e)

# entity query for all LINE entities in modelspace
for e in msp.query('LINE'):
    print_entity(e)
def layer_and_color_key(entity):
    # return None to exclude entities from result container
    if entity.dxf.layer == '0':  # exclude entities from default layer '0'
        return None
    else:
        return entity.dxf.layer, entity.dxf.color

group = msp.groupby(key=layer_and_color_key)
for key, entities in group.items():
    print(f'Grouping criteria "{key}" matches following entities:')
    for entity in entities:
        print('    {}'.format(str(entity)))
    print('-'*40)
doc.saveas("solid_hatch_polyline_path.dxf")